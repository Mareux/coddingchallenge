import React, {useState} from 'react';

function Form(props) {

    const [item, setItem] = useState(props.item);

    const handleFormSubmit = (event) => {
        event.preventDefault();
        props.onItemChange(item);
    };

    return (
        <div>
            <form onSubmit={handleFormSubmit}>
                <input value={item.artist_id}
                       disabled={true}/>
                <input value={item.artist_name}
                       onChange={event => setItem(p => ({...p, artist_name: event.target.value}))} required={true}
                       maxLength={50}/>
                <input value={item.artist_country} disabled={true}/>
                <input value={item.artist_rating}
                       onChange={event => setItem(p => ({...p, artist_rating: event.target.value}))} required={true}/>
                <a><input value={item.artist_twitter_url} disabled={true}/></a>
                <textarea value="" cols={40} rows={5}></textarea>
                <button type="reset">Cancel</button>
                <button type="submit">Save</button>
            </form>
        </div>
    );
}

export default Form;
