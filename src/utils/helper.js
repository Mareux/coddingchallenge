export function getJSONFromLocalStorage() {
    return JSON.parse(localStorage.getItem("music") || '[]')
}

export function setJsonToLocalStorage(file) {
    localStorage.setItem("music", JSON.stringify(file))
}

export function getItemById(items, id) {
    return (items.artist_list.find(items => items.artists.artist_id === id));
}
