import React, {useState} from 'react';
import {getJSONFromLocalStorage, setJsonToLocalStorage, getItemById} from './utils/helper.js'
import {string} from "./utils/JSON";
import Form from "./components/Form";

function App() {

    let musicData = getJSONFromLocalStorage();

    if (!musicData.length) {
        musicData = JSON.parse(string);
        // const tmpMusicData = musicData.artist_list.map(item => {
        //     return ({...item, comment: ""})
        // });

        setJsonToLocalStorage(musicData);
    }

    const [currentPage, setPage] = useState(0);
    const [items, setItems] = useState(musicData);
    const [form, setFormState] = useState(false);

    const onItemChange = (item) => {
    };

    const showForm = () => {
        setFormState(true);
    };

    const drawTableItems = () => {
        return items.artist_list.slice(currentPage * 10, currentPage * 10 + 10).map((item) => {
            const {artist_id, artist_name, artist_country, artist_rating, updated_time} = item.artist;
            return (
                <tr key={artist_id}>
                    <td onClick={showForm}>{artist_id}</td>
                    <td>{artist_name}</td>
                    <td>{artist_country}</td>
                    <td>{artist_rating}</td>
                    <td>{updated_time}</td>
                </tr>
            );
        })
    };

    const drawPageNumbers = () => {
        let numbers = [];

        for (let i = 0; i < items.artist_list.length / 10; i++) {
            numbers.push(i + 1);
        }

        return (numbers.map(items => {
            return (<button className="pages" key={items} value={items}
                            onClick={event => setPage(event.target.value - 1)}>{items}</button>)
        }))
    };

    const handleSearch = (event) => {
        event.preventDefault();
        //
        // const newItems = items.artist_list.filter(item =>{
        //    return (item.artist.artist_name.includes(event.target.value))}
        // );
        //
        // setItems(newItems);
    };

    console.log(items);

    return (
        <>
            <div>
                <input placeholder="Search by name" onInput={handleSearch}/>
                <table>
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Country</th>
                        <th>Rating</th>
                        <th>Update Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    {drawTableItems()}
                    </tbody>
                </table>
                {drawPageNumbers()}
            </div>
            {/*{form && <Form item={getItemById(items, )} onItemChange={onItemChange}/>}*/}
        </>
    );
}

export default App;
